## Which is what?
- `driver_gaze_tracker_histogram.py`
  Converts the gaze tracker diagram at the bottom of the image into a discrete signal.

- `driver_numerical_diagrams_using_axis_center.py`
  OCRs the numerical diagrams (Perimetry diagram + total and pattern deviation diagrams)

- `grid_piece_utils.py`
  For cutting diagrams into a grid of pieces.

- `cutout.py`
  Contains all functions that use Cropping to achieve their goal.

- `find_lines.py`
  For finding the axis of diagrams using Hough transform and Canny filter.

- `hocr_utils.py`
  Utilities for XML parsing of hocr output of Tesseract.

- `pipeutils.py`
  Stuff to make `sspipe` more bearable.

- `ocr_page.py`
  Primitive functions used in pre-procssing the image.
  It's a bit convoluted and has a lot of unrelated stuff.

---

##### TODO
- Add desc for other .py files.