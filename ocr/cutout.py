from types import SimpleNamespace
from typing import List

import numpy as np


def cut_main(imgnp: np.ndarray) -> np.ndarray:
    img = imgnp[1100:2900, 1100:2800].copy()
    empty = np.percentile(img, 97)
    img[:350, :350] = empty
    img[-350:, :350] = empty
    img[-350:, -350:] = empty
    img[:350, -350:] = empty
    img[-150:, :700] = empty
    return img

def cut_tdev(imgnp: np.ndarray) -> np.ndarray:
    img = imgnp[2737:3790, 744:1930].copy()
    # empty = np.percentile(img, 97)
    return img

def cut_pdev(imgnp: np.ndarray) -> np.ndarray:
    img = imgnp[2737:3790, 2244:3300].copy()
    # empty = np.percentile(img, 97)
    return img

def cut_gaze(imgnp: np.ndarray) -> np.ndarray:
    # img = imgnp[5280:5610, 407:4378].copy()
    img = imgnp[5530:5880, 500:4578].copy()
    # empty = np.percentile(img, 97)
    return img


def cutout_into_piece_coords(shape, cell_size, xy, center=None) -> List[SimpleNamespace]:
    if center is None:
        center = (shape[0] // 2, shape[1] // 2)
    x, y = xy
    coords = [None for i in range((2 * x) * (2 * y))]
    idx = 0
    for i in range(-x + 1, x + 1):
        for j in range(-y + 1, y + 1):
            upper = center.y - cell_size[0] * i
            lower = upper + cell_size[0]
            left =  center.x - cell_size[1] * j
            right = left + cell_size[1]
            coords[idx] = SimpleNamespace(x=(upper, lower), y=(left, right))
            idx += 1
    coords.reverse()
    return coords
