#!/usr/bin/env python3
import logging
from pathlib import Path
from typing import Callable

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from natsort import natsorted
from scipy import ndimage as ndi
from skimage import color, morphology, transform
from sspipe import p, px
from tqdm import tqdm

from cutout import cut_gaze
from find_lines import find_vertical_line
from ocr_page import (fix_exposure, load_image_bw, opening_image,
                      threshold_image)
from pipeutils import p_print, p_show_img, plt_show_image

TESTING = False

LOG_PATH = './log/gaze/driver.0.0.2.log'
# LOG_PATH = '/dev/null'
# LOG_PATH = "/proc/self/fd/1"

OUTPUT_DIR = "./OUTPUT_GAZE_TARCKER/"


def gen_filename_for_NEW(path: Path, suffix: str = OUTPUT_DIR) -> Path:
    return Path(suffix) / Path(f'{path.parent.name.replace(" ", "_")}_{path.stem + ".csv"}')


def gen_filename_for_old(path: Path, suffix: str = OUTPUT_DIR) -> Path:
    return Path(suffix) / (path.stem + '.csv')


def ocr_gaze_and_save(path: Path, gen_filename: Callable[[Path, str], Path]) -> bool:
    output_filepath: Path = gen_filename(path)

    if output_filepath.exists():
        logging.info(f'SKIPPING {output_filepath}, file already exists')
        return True

    n_resize = 2

    def diag_fix_exposure(img):
        return fix_exposure(img, percentiles=(2, 10))

    def resize_by_n(img):
        return transform.resize(
            img,
            (img.shape[0] // n_resize, img.shape[1] // n_resize),
            anti_aliasing=True,
        )

    def preprocess_image(img):
        return (
            img
            | p(diag_fix_exposure)  # | p(resize_by_n) | p(threshold_image)
            # | p(lambda x: tee(x, lambda z: print(z.shape)))  # prints shape
            # | p(lambda x: opening_image(x, morphology.square(2)))
            | p(lambda im: ndi.gaussian_filter(im, 1))
            # | p_show_img
        )

    logging.info(f"Processing {path}")

    logging.info(f"\tLoading {path} ...")
    main_image = load_image_bw(path)
    logging.info(f"\tLoading done.")

    logging.info("\tPre-processing image...")
    im_bottom = main_image | p(cut_gaze) | p(preprocess_image)
    logging.info("\tPre-processing done.")

    # # Find the top-line's slope
    # logging.info("\tFinding slope...")
    # im_topline = bottom_image[:100, 500:-500]
    # line = find_vertical_line(im_topline)
    # print(line.slope)
    # plt.imshow(im_topline, cmap='gray')
    # plt.show()
    # logging.info("\tFinding slope done.")
    # return

    logging.info("\tCut-out gaze trakcer...")
    im_gaze = (
        im_bottom[100:-100].copy() |
        p(threshold_image) |
        p(lambda x: opening_image(x, morphology.square(1)))
    )
    logging.info("\tCut-out gaze trakcer done.")

    # Horizontal histogram on gaze tracker
    logging.info("\tCalcing horizontal histogram...")
    baseline = im_gaze.sum(axis=1).argmin()
    logging.info(f'\t\tBaseline row idx: {baseline}')
    # # show stuff
    # backup = im_gaze[baseline].copy()
    # im_gaze[baseline] = 255
    # plt.imshow(im_gaze, cmap='gray')
    # plt.show()
    # im_gaze[baseline] = backup
    logging.info('\tCalcing horizontal histogram done.')

    # return

    above = (1 - im_gaze[:baseline]).sum(axis=0)
    below = (1 - im_gaze[baseline+1:]).sum(axis=0)
    select_above = above > below
    sign = 2 * select_above - 1
    gaze = sign * np.where(select_above, above, below)

    # show the gaze signal
    # # mabove = (select_above * below)[1300:1600]
    # # mabove = gaze[1000:1800]
    # mabove = gaze
    # plt.figure(figsize=(16,4))
    # # plt.plot(range(len(mabove)), sign[1300:1600] * mabove)
    # plt.plot(range(len(mabove)), mabove)
    # plt.show()

    # print(output_filepath)
    df = pd.DataFrame({output_filepath.name: gaze})
    df.to_csv(output_filepath, index=False)
    logging.info(f"Finished, {output_filepath}")

    return True


def mkdir():
    Path.mkdir(Path(LOG_PATH).parent, exist_ok=True)
    Path.mkdir(Path(OUTPUT_DIR), exist_ok=True)


def main():
    from itertools import chain

    mkdir()

    if TESTING:
        logging.basicConfig(
            filename='/proc/self/fd/1',
            level=logging.INFO,
        )
        ocr_gaze_and_save(
            Path('/home/luce/workspace/artin/optimetri/DATA/Cropped/P000.jpg'), OUTPUT_DIR)
    else:
        logging.basicConfig(
            filename=LOG_PATH,
            filemode='w+',
            level=logging.INFO,
        )
        img_paths_old = chain(
            Path('/run/media/luce/4c56d5f8-d2fe-429c-86a2-c527a68f22f1/DATA/Cropped') | px.glob('**/*.jpg'),
            Path('/run/media/luce/4c56d5f8-d2fe-429c-86a2-c527a68f22f1/DATA/Cropped2') | px.glob('**/*.jpg'),
        ) | p(natsorted)

        assert all(map(
            lambda p: ocr_gaze_and_save(p, gen_filename=gen_filename_for_old),
            tqdm(img_paths_old)
        ))

        img_paths_new = (
            Path('/run/media/luce/4c56d5f8-d2fe-429c-86a2-c527a68f22f1/DATA/ScanNEW')
            | px.glob('**/*.jpg')
            | p(natsorted)
        )
        assert all(map(
            lambda p: ocr_gaze_and_save(p, gen_filename=gen_filename_for_NEW),
            tqdm(img_paths_new)
        ))


if __name__ == "__main__":
    main()
