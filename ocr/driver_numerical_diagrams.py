#!/usr/bin/env python3
from pathlib import Path
from natsort import natsorted
from sspipe import p, px
from tqdm import tqdm
import logging

import numpy as np
from skimage import morphology, transform, color
import matplotlib.pyplot as plt

from cutout import cut_main, cut_pdev, cut_tdev
from pipeutils import p_print, p_show_img, plt_show_image

from ocr_page import (
    load_image_bw,
    cut_top,
    cut_right,
    fix_exposure,
    threshold_image,
    opening_image,
    closing_image,
    ocr_image_top,
    ocr_image_right,
    parse_ocr_str,
    save2csv,
    save2text,
    show_image
)

from hocr_utils import get_bbox_from_hocr


# LOG_PATH = 'log/driver.0.1.0.log'
# LOG_PATH = '/dev/null'
LOG_PATH = '/proc/self/fd/1'

def ocr_diags_and_save(path: Path, suffix: str = '') -> bool:
    def diag_fix_exposure(img): return fix_exposure(img, percentiles=(2, 10))
    n_resize = 2
    def resize_by_n(img): return transform.resize(
        img, (img.shape[0] // n_resize, img.shape[1] // n_resize), anti_aliasing=True)

    pre_img = (
        path
        | p(load_image_bw) | p(cut_main)
        | p(diag_fix_exposure) | p(resize_by_n) | p(threshold_image)
        | p(lambda x: tee(x, lambda z: print(z.shape)))  # prints shape
        | p(lambda x: opening_image(x, morphology.square(2)))
        #  | p_show_img
    )

    hocr_xml = (pre_img | p(ocr_image_right))

    # jesus christ # FIXME
    pre_colored = np.repeat(pre_img.astype(np.uint8(255))[:, :, np.newaxis], 3, axis=2)

    bbox_value = get_bbox_from_hocr(hocr_xml)
    for bbox, value in bbox_value.items():
        y = (bbox[0] + bbox[2]) // 2
        x = (bbox[1] + bbox[3]) // 2
        pre_colored[x, y] = [255, 0, 255]
        pre_img[x, y] = 69

    print(pre_colored.dtype, pre_colored.shape)
    np.savetxt('preimg.txt', pre_img, fmt='%1.0f')
    print(pre_colored[x-1, y])

    # show_image(pre_colored, force_vis=True)
    # plt.show()

    

    # with open('output.xml', 'w') as fp:
    #     fp.writelines(ocr_list.decode('utf-8'))
    exit(0)
    if ocr_list:
        save2csv(path, ocr_list, suffix=suffix)
    else:
        logging.info(f"::{path}::WOW it's fucking nothing")
    return True


def main():
    logging.basicConfig(filename=LOG_PATH,
                        # filemode='w',
                        level=logging.INFO)

    # img_paths = Path('/run/media/luce/7AFA92DC327E1C3A/DATA/ScanNEW/') \
    #     | px.glob('**/*.jpg') \
    #     | p(natsorted)

    ocr_diags_and_save(Path(
        '/home/luce/workspace/artin/optimetri/DATA/ScanNEW/New folder (8)/Untitled-3.jpg'))
    # assert all(map(ocr_top_save, tqdm(img_paths[:1])))
    # assert all(map(lambda x: ocr_right_save(
    #     x, suffix='.right'), tqdm(img_paths[:])))


if __name__ == '__main__':
    main()
