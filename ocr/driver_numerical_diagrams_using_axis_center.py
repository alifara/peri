#!/usr/bin/env python3
import logging
from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pytesseract as tess
from natsort import natsorted
from scipy import ndimage as ndi
from skimage import color, morphology, transform
from sspipe import p, px
from tqdm import tqdm

from cutout import cut_main, cut_pdev, cut_tdev, cutout_into_piece_coords

# from hocr_utils import get_bbox_from_hocr
from grid_piece_utils import (
    columnize_image_grid,
    columnize_image_grid_and_show,
    find_non_emtpy_pieces,
    find_pieces_coords,
    show_grid_of_pieces,
)
from ocr_page import (
    closing_image,
    cut_right,
    cut_top,
    fix_exposure,
    load_image_bw,
    ocr_image_right,
    ocr_image_top,
    opening_image,
    parse_ocr_str,
    save2csv,
    save2text,
    show_image,
    threshold_image,
)
from pipeutils import p_print, p_show_img, plt_show_image

LOG_PATH = './log/main_diag/driver.0.0.1.log'
# LOG_PATH = '/dev/null'
# LOG_PATH = "/proc/self/fd/1"

OUTPUT_DIR = "./OUTPUT_MAIN_DIAG/"


def ocr_mini_diagram_sth_dev(im, cell_size):
    pieces = find_pieces_coords(im, cell_size)
    show_grid_of_pieces(im, pieces)
    # return

    grid = np.empty(10 * 10, dtype=str)
    grid.shape = (10, 10)

    idx = -1
    for i, j in tqdm(np.ndindex(10, 10), total=100):
        idx += 1
        piece = pieces[idx]
        xl, xr = piece.x
        yl, yr = piece.y
        imp = im[xl:xr, yl:yr]
        if imp.std() < 3:  # if piece is empty
            continue
        # imp = imp | p(threshold_image) #| p_show_img
        value = tess.image_to_string(imp, config="--psm 11 --oem 0")
        # value = tess.image_to_string(imp, config='--psm 11 --oem 2 -c tessedit_char_whitelist=0123456789\- ')
        try:
            value = int(value)
        except ValueError:
            pass
        finally:
            grid[i][j] = value
            print(grid)
    return grid


def find_piece_idx_from_top(top: int, piece_height: int, gap_size: int) -> int:
    """Given the `top` property, find which piece it belongs to in 
 the column image."""
    return top // (piece_height + gap_size)


def ocr_diags_and_save(path: Path, suffix: str = "") -> bool:
    n_resize = 2

    def diag_fix_exposure(img):
        return fix_exposure(img, percentiles=(2, 10))

    def resize_by_n(img):
        return transform.resize(
            img,
            (img.shape[0] // n_resize, img.shape[1] // n_resize),
            anti_aliasing=True,
        )

    def preprocess_image(img):
        return (
            img
            | p(diag_fix_exposure)  # | p(resize_by_n) | p(threshold_image)
            # | p(lambda x: tee(x, lambda z: print(z.shape)))  # prints shape
            # | p(lambda x: opening_image(x, morphology.square(2)))
            | p(lambda im: ndi.gaussian_filter(im, 1))
            # | p_show_img
        )

    logging.info(f"Processing {path}")

    logging.info(f"\tLoading {path} ...")
    main_image = load_image_bw(path)
    logging.info(f"\tLoading done.")

    logging.info("\tPre-processing image...")
    # im_tdev = main_image | p(cut_tdev) | p(preprocess_image)
    # im_pdev = main_image | p(cut_pdev) | p(preprocess_image)
    im_main = main_image | p(cut_main) | p(preprocess_image)
    logging.info("\tPre-processing done.")

    main_diagram_piece_size = (148, 148)

    # Not really a good idea cause the Tesseract's LSTM backend will
    # not work well and have sub-par performance.
    # Just OCR each piece separatley
    ## grid_tdev = ocr_mini_diagram_sth_dev(im_tdev, cell_size=(103, 96))
    ## grid_pdev = ocr_mini_diagram_sth_dev(im_pdev, cell_size=(103, 96))
    ## grid_main = ocr_mini_diagram_sth_dev(im_main, cell_size=(148, 148))

    # Find coords of each piece
    logging.info("\tFinding pieces...")
    pieces_main = find_pieces_coords(
        im_main, cell_size=main_diagram_piece_size
    )
    logging.info("\tFinding pieces done.")

    # Find Non empty pieces in the grid
    logging.info("\tFiltering-out empty pieces...")
    non_empty_pieces_main, non_empty_piece_coords = find_non_emtpy_pieces(
        im_main, pieces_main
    )
    logging.info("\tFiltering-out empty pieces done.")
    logging.info(f"\tNumber of non-empty pieces: {len(non_empty_pieces_main)}")

    # Resize the grid into a column and the run OCR on on the column
    gap_size = 90
    im_main_column = columnize_image_grid(
        im_main, non_empty_pieces_main, gap_size=gap_size, gap_value=255,
    )
    logging.info("\tCreated column image.")

    logging.info("\tStarting Tesseract...")
    # value = tess.image_to_string(im_main_column, config="--psm 4 --oem 2")
    # value = tess.image_to_string(
    #     im_main_column, config="--psm 4 --oem 2 -c tessedit_char_whitelist=0123456789"
    # )
    # hocr_xml = tess.image_to_pdf_or_hocr(
    #     im_main_column, config="--psm 4 --oem 2 -c tessedit_char_whitelist=0123456789", extension='hocr')
    tess_data = tess.image_to_data(
        im_main_column,
        output_type=tess.Output.DICT,
        config="--psm 4 --oem 2 -c tessedit_char_whitelist=0123456789"
    )
    logging.info("\tTesseract finished.")

    # # Show the grid which is fit on the diagram
    # show_grid_of_pieces(im_main, pieces_main)
    # plt.savefig("Figure_Grid.png")

    # # Show the column image made from omitting emtpy pieces (or 'cells') of the grid
    # matplotlib.image.imsave('Figure_Column.png', im_main_column, cmap='gray')

    # columnize_image_grid_and_show(im_main, pieces_main)

    # Save the diagrams in grid-like .csv file.
    table = np.full((10, 10), '', dtype=object)
    for top, text in zip(tess_data['top'], tess_data['text']):
        if text:
            column_piece_idx = find_piece_idx_from_top(
                top=top, piece_height=main_diagram_piece_size[0], gap_size=gap_size
            )
            i, j = non_empty_piece_coords[column_piece_idx]
            table[i, j] = text

    output_filepath = Path(suffix) / (path.stem + '.csv')
    np.savetxt(output_filepath, table, delimiter=',', fmt="%s")

    logging.info(f"Finished, {output_filepath}")

    return True


def mkdir():
    Path.mkdir(Path(LOG_PATH).parent, exist_ok=True)
    Path.mkdir(Path(OUTPUT_DIR), exist_ok=True)


def main():
    mkdir()

    logging.basicConfig(
        filename=LOG_PATH,
        filemode='w+',
        level=logging.INFO,
    )

    img_paths = (
        Path('/run/media/luce/4c56d5f8-d2fe-429c-86a2-c527a68f22f1/DATA/Cropped')
        | px.glob('**/*.jpg')
        | p(natsorted)
    )

    assert all(map(
        lambda p: ocr_diags_and_save(p, suffix=OUTPUT_DIR),
        tqdm(img_paths)
    ))


if __name__ == "__main__":
    main()
