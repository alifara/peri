#!/usr/bin/env python3
from pathlib import Path
from natsort import natsorted
from sspipe import p, px
from tqdm import tqdm
import logging

from skimage import morphology
from ocr_page import (
    load_image_bw,
    cut_top,
    cut_right,
    fix_exposure,
    threshold_image,
    opening_image,
    closing_image,
    ocr_image_top,
    ocr_image_right,
    parse_ocr_str,
    save2csv,
    save2text,
)


import matplotlib.pyplot as plt
from ocr_page import show_image

# LOG_PATH = 'log/driver.0.1.0.log'
# LOG_PATH = '/dev/null'
LOG_PATH = '/proc/self/fd/1'


def tee(a, f):
    f(a)
    return a


def plt_show_image(x):
    show_image(x, force_vis=True)
    plt.show()


p_show_img = p(lambda x: tee(x, plt_show_image))
p_print = p(lambda x: tee(x, print))


def ocr_top_save(path: Path, suffix: str = '') -> bool:
    ocr_list = path \
        | p(load_image_bw) \
        | p(cut_top) \
        | p(fix_exposure) \
        | p(threshold_image) \
        | p(lambda x: opening_image(x, morphology.square(5))) \
        | p_show_img \
        | p(ocr_image_top) \
        | p(lambda x: tee(x, logging.info)) \
        | p(parse_ocr_str) \
        | p(lambda x: tee(x, logging.info))
    exit(0)
    if ocr_list:
        save2csv(path, ocr_list, suffix=suffix)
    else:
        logging.info(f"::{path}::WOW it's fucking nothing")
    return True


def ocr_right_save(path: Path, suffix: str = '') -> bool:
    assert path \
        | p(load_image_bw) \
        | p(cut_right) \
        | p(lambda x: fix_exposure(x, (2, 3))) \
        | p(lambda x: opening_image(x, morphology.square(3))) \
        | p(threshold_image) \
        | p(ocr_image_right) \
        | p(lambda x: save2text(path, x, suffix=suffix))

    # | p(lambda x: tee(x, logging.info)) | p(parse_ocr_str)
    # if ocr_list:
    #     save2csv(path, ocr_list, suffix=suffix)
    # else:
    #     logging.info(f"::{path}::WOW it's fucking nothing")
    return True


def main():
    logging.basicConfig(filename=LOG_PATH,
                        # filemode='w',
                        level=logging.INFO)

    img_paths = Path('/run/media/luce/7AFA92DC327E1C3A/DATA/ScanNEW/') \
        | px.glob('**/*.jpg') \
        | p(natsorted)

    ocr_top_save(Path(
        '/run/media/luce/7AFA92DC327E1C3A/DATA/ScanNEW/New folder (2)/Untitled-36.jpg'))
    # assert all(map(ocr_top_save, tqdm(img_paths[:1])))
    # assert all(map(lambda x: ocr_right_save(
    #     x, suffix='.right'), tqdm(img_paths[:])))


if __name__ == '__main__':
    main()
