
from types import SimpleNamespace

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from skimage import data
from skimage.draw import line
from skimage.feature import canny
from skimage.transform import hough_line, hough_line_peaks


class Line:
    # This init function is way too ugly and handles onley one case but it works.
    def __init__(self, slope=None, x0=None, y0=None, angle=None):
        self.angle = angle
        self.x0 = x0
        self.y0 = y0

        self.slope = slope if slope is not None else np.tan(angle + np.pi/2)


def _find_lines_with_tested_angles(image, tested_angles) -> Line:
    # Classic straight-line Hough transform
    # tested_angles = np.linspace(np.pi/2-.2, np.pi/2 +.2, 16, endpoint=False)  # horizontal lines
    h, theta, d = hough_line(image, theta=tested_angles)

    # Get slope and xy of line
    _, angles, dists = hough_line_peaks(h, theta, d)
    angle = np.median(angles)
    dist = np.median(dists)

    (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
    return Line(angle=angle, x0=x0, y0=y0)


def _find_vertical_line(image, count=6) -> Line:
    tested_angles_v = np.linspace(-.1, .1, count,
                                  endpoint=False)  # vertical lines
    return _find_lines_with_tested_angles(image, tested_angles_v)


def _find_horizontal_line(image, count=6) -> Line:
    tested_angles_h = np.linspace(
        np.pi/2-.2, np.pi/2 + .2, count, endpoint=False)  # horizontal lines
    return _find_lines_with_tested_angles(image, tested_angles_h)


def _find_intersection(l1: Line, l2: Line):
    b1 = l1.y0 - (l1.slope * l1.x0)
    b2 = l2.y0 - (l2.slope * l2.x0)
    da = (l1.slope - l2.slope)
    assert np.abs(da) > 10 ** -6
    x = (b2 - b1) / da
    y = l1.slope * x + b1
    return SimpleNamespace(x=x, y=y)


def find_vertical_line(image: np.ndarray, count: int = 12) -> Line:
    im = canny(image, sigma=3)
    # plt.imshow(im, cmap='gray')
    # plt.show()
    return _find_vertical_line(im, count=12)


def find_center_of_diagram(image):
    im = canny(image, sigma=3)
    l_horiz = _find_horizontal_line(im)
    l_vert = _find_vertical_line(im)
    return _find_intersection(l_horiz, l_vert)


def find_lines_testing(image):
    # Classic straight-line Hough transform
    tested_angles = np.linspace(
        np.pi/2-.2, np.pi/2 + .2, 16, endpoint=False)  # horizontal lines
    # tested_angles = np.linspace(-.1, .1, 8, endpoint=False) # vertical lines
    h, theta, d = hough_line(image, theta=tested_angles)

    # Generating figure 1
    fig, axes = plt.subplots(1, 3, figsize=(15, 6))
    ax = axes.ravel()

    ax[0].imshow(image, cmap=cm.gray)
    ax[0].set_title('Input image')
    ax[0].set_axis_off()

    angle_step = 0.5 * np.diff(theta).mean()
    d_step = 0.5 * np.diff(d).mean()
    bounds = [np.rad2deg(theta[0] - angle_step),
              np.rad2deg(theta[-1] + angle_step),
              d[-1] + d_step, d[0] - d_step]
    ax[1].imshow(np.log(1 + h), extent=bounds, cmap=cm.gray, aspect=1 / 1.5)
    ax[1].set_title('Hough transform')
    ax[1].set_xlabel('Angles (degrees)')
    ax[1].set_ylabel('Distance (pixels)')
    ax[1].axis('image')

    ax[2].imshow(image, cmap=cm.gray)
    ax[2].set_ylim((image.shape[0], 0))
    ax[2].set_axis_off()
    ax[2].set_title('Detected lines')

    # Get slope and xy of line
    _, angles, dists = hough_line_peaks(h, theta, d)
    angle = np.median(angles)
    dist = np.median(dists)

    (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
    ax[2].axline((x0, y0), slope=np.tan(angle + np.pi/2))

    plt.tight_layout()
    plt.show()
