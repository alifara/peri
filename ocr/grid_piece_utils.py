from types import SimpleNamespace
from typing import List, Tuple, Union, Callable

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid

from cutout import cutout_into_piece_coords
from find_lines import find_center_of_diagram

THRESHOLD_STD_PEICE = 5


def show_grid_of_pieces(im: np.ndarray, pieces: List[SimpleNamespace]):
    # griding the pieces
    fig = plt.figure(figsize=(10, 10))
    grid = ImageGrid(
        fig,
        111,  # similar to subplot(111)
        nrows_ncols=(10, 10),
        axes_pad=0.0,
        share_all=True,
    )
    for piece, ax, (i, j) in zip(pieces, grid, np.ndindex(10, 10)):
        # print(piece)
        # print(im.shape)
        xl, xr = piece.x
        yl, yr = piece.y
        i2 = im[xl:xr, yl:yr]
        # print(f'{(i, j)}.std: {i2.std()}')
        ax.imshow(i2, cmap="gray")
        ax.set_xticks([])
        ax.set_yticks([])


def find_pieces_coords(
    im: np.ndarray, cell_size: Tuple[int, int]
) -> List[SimpleNamespace]:
    center = find_center_of_diagram(im)
    center.x = round(center.x)
    center.y = round(center.y)
    return cutout_into_piece_coords(im.shape, cell_size, (5, 5), center=center)


def _piece_is_empty(im: np.ndarray, piece: SimpleNamespace) -> bool:
    xl, xr = piece.x
    yl, yr = piece.y
    i2 = im[xl:xr, yl:yr]
    return i2.std() > THRESHOLD_STD_PEICE


def _piece_is_empty_box_inthe_middle(im: np.ndarray, piece: SimpleNamespace, d: int = 10) -> bool:
    xl, xr = piece.x
    yl, yr = piece.y
    i2 = im[xl+d:xr-d, yl+d:yr-d]
    return i2.std() > THRESHOLD_STD_PEICE


def find_non_emtpy_pieces(
    im: np.ndarray, 
    pieces: List[SimpleNamespace], 
    piece_is_empty: Callable[[np.ndarray, SimpleNamespace], bool] = _piece_is_empty_box_inthe_middle
) -> List[SimpleNamespace]:
    non_empty_pieces = filter(lambda piece_idxs:
                                 piece_is_empty(im, piece_idxs[0]), 
                              zip(pieces, np.ndindex(10, 10)))
    return list(zip(*non_empty_pieces))


def columnize_image_grid_and_show(
    im: np.ndarray, pieces: List[SimpleNamespace]
) -> np.ndarray:
    piece_count = len(pieces)

    fig = plt.figure(figsize=(2, piece_count // 2))
    grid = ImageGrid(
        fig,
        111,  # similar to subplot(111)
        nrows_ncols=(piece_count, 1),
        axes_pad=0.0,
    )
    for piece, ax in zip(pieces, grid):
        # print(piece)
        # print(im.shape)
        xl, xr = piece.x
        yl, yr = piece.y
        i2 = im[xl:xr, yl:yr]
        # print(f'{(i, j)}.std: {i2.std()}')
        ax.imshow(i2, cmap="gray")
        ax.set_xticks([])
        ax.set_yticks([])

    plt.show()


def columnize_image_grid(
    im: np.ndarray,
    pieces: List[SimpleNamespace],
    gap_size: int = 0,
    gap_value: Union[int, float] = 0,
) -> np.ndarray:
    cell_size = SimpleNamespace(
        r=abs(pieces[0].x[0] - pieces[0].x[1]), c=abs(pieces[0].y[0] - pieces[0].y[1])
    )
    piece_count = len(pieces)

    row_len = cell_size.r * piece_count + gap_size * max(0, (piece_count - 1))
    im_column = np.full((row_len, cell_size.c), gap_value, dtype=im.dtype)
    for piece, idx in zip(pieces, range(0, row_len, cell_size.r + gap_size)):
        xl, xr = piece.x
        yl, yr = piece.y
        i2 = im[xl:xr, yl:yr]
        # im_column[idx: 
        #           idx + cell_size.r + gap_size, :] = gap_value
        im_column[idx: idx + i2.shape[0], :i2.shape[1]] = i2

    return im_column
