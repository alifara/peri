from bs4 import BeautifulSoup


def get_bbox_coords(tag_corx_word):
    title = tag_corx_word.attrs['title']
    return title[:title.find(';', 1)]


def get_bbox_from_hocr(xml_input: bytes):
    soup: BeautifulSoup = BeautifulSoup(xml_input, 'lxml-xml')

    # find ocr capabilities and make sure it has 'ocrx_word'
    ocr_cap = soup.head.findAll(
        'meta', {'name': 'ocr-capabilities'})[0].attrs['content'].split()
    assert 'ocrx_word' in ocr_cap

    # extract all span tags with 'ocrx_word'
    all_tags = soup.body.findAll('span', {'class': 'ocrx_word'})

    # mapping of {bbox-coords[Tuple[4, int]] → ocr_str[str]}
    bbox = dict(map(lambda tag: (tuple(map(int,
                                           get_bbox_coords(tag)[5:].split())),
                                 tag.string.strip()),
                    all_tags))
    # I couldn't int() the tag.string because the `30' to the left of
    # the x-axis is combined with the next next to it, either the one
    # above or below the x-axis.
    return bbox


def main():
    with open('output.xml', 'r') as fp:
        xml = fp.read().encode()
    get_bbox_from_hocr(xml)


if __name__ == "__main__":
    main()
