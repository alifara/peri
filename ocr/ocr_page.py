# coding: utf-8

import csv
import logging
from pathlib import Path
from typing import List, Optional, Sequence, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pytesseract as tess
import pyvips
import skimage
from natsort import natsorted
from skimage import color, data, exposure, filters, io, morphology, util
from sspipe import p, px

# From sspipe's doc:
#   The whole functionality of this library is exposed by two objects
#   p (as a wrapper for functions to be called on the piped object) and
#   px (as a placeholder for piped object).


# In[186]:


# img_paths = Path('/run/media/luce/7AFA92DC327E1C3A/DATA/ScanNEW/') | px.glob('**/*.jpg') | p(natsorted)
# len(img_paths)
# # !pip install natsort
# i = 5


# In[204]:


# image_path = img_paths[i]
# i += 1

SHOULD_VIS = False


# In[205]:

def show_image(image: np.ndarray, title: str = 'Image', cmap='gray', imsize=(20, 20), plt=plt, force_vis=False) -> None:
    if not SHOULD_VIS and not force_vis:
        return
    plt.figure(figsize=imsize)
    if image[0][0] is not np.ndarray:
        plt.imshow(image, cmap=cmap)
    else:
        plt.imshow(image)
    plt.title(title)
    plt.axis('off')


def show_histogram(image: np.ndarray, title: str = 'historgram', thresh: int = None, plt=plt, force_vis=False) -> None:
    if not SHOULD_VIS and not force_vis:
        return
    plt.hist(image.ravel(), bins=256)
    plt.title(title)
    if thresh is not None:
        plt.axvline(thresh, color='r')
# ## Load image

# In[206]:


# imgnp = io.imread(image_path)


# In[207]:

def load_image_bw(image_path: Path) -> np.ndarray:
    image = pyvips.Image.new_from_file(
        str(image_path), access="sequential")  # , shrink=4)
    logging.info(image)
    if image.bands == 1:
        image_bw = image
    else:
        image_bw = image.sRGB2scRGB().scRGB2BW(depth=8)
        logging.info(image_bw)
    mem_img = image_bw.write_to_memory()
    # imgnp = np.frombuffer(mem_img, dtype=np.uint8).reshape(image_bw.height, image_bw.width, 3)  # sRGB
    imgnp = np.frombuffer(mem_img, dtype=np.uint8).reshape(
        image_bw.height, image_bw.width)  # BW
    return imgnp


# In[208]:


# show_image(imgnp, force_vis=True)


# ## Cut-out the top part

# In[209]:


# top = util.img_as_ubyte(imgnp[100:1600, 500:-500]).copy()
def cut_top(imgnp: np.ndarray) -> np.ndarray:
    top = imgnp[100:1600, 500:-500].copy()
    top[1000:, 1000:] = np.percentile(top, 97)
    show_image(top)
    return top


# ## Cut-out right half

# In[241]:

def cut_right(imgnp: np.ndarray) -> np.ndarray:
    right = imgnp[2875:4300, 3400:-300].copy()
    show_image(right)
    return right

# ## Convert to gray-Scale

# In[210]:


# ## imgray = skimage.color.rgb2gray(image)
# # topgray = color.rgb2gray(top)
# # topgray = util.img_as_ubyte(topgray)
# topgray = top
# show_image(topgray);


# ## Adjust the image using *local histogram equalization*

# In[211]:


def plot_hist(image, ax_hist, bins=256, force=False):
    """Plot an image along with its histogram and cumulative histogram.
    """
    if not SHOULD_VIS and not force:
        return
    ax_hist = plt
    ax_cdf = ax_hist.twinx()

    # Display histogram
    ax_hist.hist(image.ravel(), bins=bins)
    ax_hist.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0))
    ax_hist.set_xlabel('Pixel intensity')

    xmin, xmax = util.dtype.dtype_range[image.dtype.type]
    ax_hist.set_xlim(xmin, xmax)

    # Display cumulative distribution
    img_cdf, bins = exposure.cumulative_distribution(image, bins)
    ax_cdf.plot(bins, img_cdf, 'r')

    return ax_hist, ax_cdf

# # fig, ax = plt.subplots()
# ax = None
# plot_hist(topgray, ax);


# ## Remove small dots using an *opening* operation

# In[212]:


# topop = morphology.opening(topgray, morphology.square(2))
# show_image(topop);


# In[213]:

def fix_exposure(img: np.ndarray, percentiles: Tuple[int, int] = (3, 8)) -> np.ndarray:
    topgray = img
    ps = np.percentile(topgray, percentiles)
    topadj = exposure.rescale_intensity(topgray, in_range=tuple(ps))
    # top_ajd = skimage.exposure.equalize_adapthist(topg)

    show_image(topadj)
    return topadj

# ## Thresholding

# In[214]:


def threshold_image(img: np.ndarray) -> np.ndarray:
    topadj = img
    otsu_filter = filters.threshold_otsu(topadj)
    topmono = topadj > (otsu_filter)
    show_image(topmono)
    return topmono


# In[215]:

def opening_image(img: np.ndarray, selem: Optional[np.ndarray]) -> np.ndarray:
    topmono = img
    topmonoopen = morphology.binary_opening(topmono, selem)
    show_image(topmonoopen)
    return topmonoopen


def closing_image(img: np.ndarray, selem: Optional[np.ndarray]) -> np.ndarray:
    topmono = img
    topmonoopen = morphology.binary_closing(topmono, selem)
    show_image(topmonoopen)
    return topmonoopen

# ## Add a black bar beore `Age`
# It's to force tesseract seperate `RX` from `Age`.

# In[216]:

def ocr_image_top(img: np.ndarray) -> str:
    topmonoopen = img
    mask = np.ones(topmonoopen.shape, dtype=bool)
    x, y = topmonoopen.shape
    left = 20
    # right = 800
    xl, xr = x // 3, 2 * x // 3
    yl = 9 * y // 11 - left
    mask[xl:xr, yl:] = False
    show_image(mask & topmonoopen)  # , force_vis=True)
    show_image(topmonoopen[xl:xr, yl:], imsize=(4, 4))  # , force_vis=True)
# In[217]:
    def ocr(img): return tess.image_to_string(img, config='--oem 2 --psm 11')
    ocr_str = ocr(topmonoopen & mask) + '\n' + ocr(topmonoopen[xl:xr, yl:])
    logging.info(f'TESS OUTPUT: {ocr_str}')
    return ocr_str
# In[218]:
# print(ocr_str)


def ocr_image_right(img: np.ndarray) -> str:
    show_image(img)  # , force_vis=True)
# In[217]:
    # def ocr(img): return tess.image_to_string(img, config='--oem 2 --psm 11')
    def ocr(img): return tess.image_to_string(img, config='--oem 2 --psm 11 -c tessedit_char_whitelist=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ')
    ocr_str = ocr(img)
    logging.info(f'TESS OUTPUT: {ocr_str}')
    return ocr_str



def ocr_image_right(img: np.ndarray) -> str:
    # show_image(img, force_vis=True)
    # def ocr(img): return tess.image_to_string(img, config='--oem 2 --psm 11')
    def ocr(img): 
        config = '--oem 2 --psm 11 -c tessedit_char_whitelist=0123456789 '
        return tess.image_to_pdf_or_hocr(img, config=config, extension='hocr')

    ocr_str = ocr(img)
    # logging.info(f'TESS OUTPUT: {ocr_str}')
    return ocr_str


# In[219]:

def parse_ocr_str(ocr_str: str) -> List[List[str]]:
    ocr_list: List[List[str]] = []
    for idx, kv_str in enumerate(filter(lambda x: x, ocr_str.splitlines())):
        l = list(map(lambda s: s.strip(' ,.\t\n'), kv_str.split(':', 1)))
        if len(l) == 1:
            l.append('')
        assert len(l) == 2
        if idx != 0 and not (l[1]) and len(l[0]) <= 5:
            ocr_list[-1][1] = f'{ocr_list[-1][1]} {l[0]}'.strip()
        else:
            ocr_list.append(l)
    # reveal_type(ocr_list)
    return ocr_list


# In[220]:


def save2csv(path: Path, ocr_list: List[Sequence[str]], suffix: str = '') -> bool:
    # with open(fpath := path / ('column.csv'), 'w') as csv_file:
    with open(fpath := path.with_suffix(f'{suffix}.column.csv'), 'w') as csv_file:
        logging.info(fpath)
        writer = csv.writer(csv_file)
        keys, values = zip(*ocr_list)  # python's unzip()
        logging.info('ok unzip')
        writer.writerow(keys)
        logging.info('ok keys')
        writer.writerow(values)
        logging.info('ok values')

    # with open(fpath := path / ('row.csv'), 'w') as csv_file:
    with open(fpath := path.with_suffix(f'{suffix}.row.csv'), 'w') as csv_file:
        logging.info(fpath)
        writer = csv.writer(csv_file)
        writer.writerow(['Key', 'Value'])
        writer.writerows(ocr_list)
        logging.info('ok rows')

    return True


def save2text(path: Path, ocr_str: str, suffix: str = '') -> bool:
    with open(fpath := path.with_suffix(f'{suffix}.txt'), 'w') as fp:
        logging.info(fpath)
        fp.writelines(ocr_str)
        logging.info('ok text')

    return True

#  char set bede
#  vocab file + (what about numbers?)
#  post process ocr
#  table of error with all things I've done and their effects
#  validation
