import matplotlib.pyplot as plt
from sspipe import p, px

from ocr_page import show_image


def tee(a, f):
    f(a)
    return a


def plt_show_image(x):
    show_image(x, force_vis=True)
    plt.show()


p_show_img = p(lambda x: tee(x, plt_show_image))
p_print = p(lambda x: tee(x, print))
