#!/usr/bin/env python3

from pathlib import Path
import pandas as pd  # Because I'm lazy
from tqdm import tqdm

skip = {'New folder (2)/Untitled-52.jpg', }


def get_name(path: Path) -> str:
    return f"{path.parent.name}/{path.stem.replace('.column', '')}.jpg"


def add_img_name(path: Path) -> bool:
    df = pd.read_csv(path)
    df['ImageName'] = get_name(path)
    df.to_csv(path, index=False)
    return True


def main():
    filenames = filter(lambda x: get_name(x) not in skip,
                       Path.cwd().glob('**/*.csv'))
    all(map(add_img_name, tqdm(filenames)))


if __name__ == '__main__':
    main()
