#!/usr/bin/env python3
from pathlib import Path
import pandas as pd  # Because I'm lazy
from tqdm import tqdm


skip = {'New folder (2)/Untitled-52.jpg', }


def get_name(path: Path) -> str:
    return f"{path.parent.name}/{path.stem.replace('.column', '')}.jpg"


def main():
    filenames = filter(lambda x: get_name(x) not in skip,
                       Path.cwd().glob('**/*.csv'))
    # all(map(add_img_name, tqdm(filenames)))
    combined_csv = pd.concat([pd.read_csv(f) for f in filenames])
    combined_csv.to_csv('./combined.csv', index=False)


if __name__ == '__main__':
    main()
