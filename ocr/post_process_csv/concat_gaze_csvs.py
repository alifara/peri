#!/usr/bin/env python3
from pathlib import Path
from sys import argv

import pandas as pd  # Because I'm lazy
from natsort import os_sorted
from sspipe import p
from tqdm import tqdm


def main() -> int:
    filenames = Path(argv[1]).glob('**/*.csv') | p(os_sorted)
    combined_csv = pd.concat([pd.read_csv(f) for f in tqdm(filenames)], axis=1)
    output_name = argv[2] if len(argv) > 2 else 'combined.csv'
    combined_csv.to_csv(Path(argv[1]) / output_name, index=False)

if __name__ == '__main__':
    exit(main())

# invocation:
# $ ./concat_gaze_csvs.py <path-to-all-gaze-csvs>/ 'combined.csv'