#!/usr/bin/env python3
from itertools import chain
from pathlib import Path
from typing import List, Tuple, Dict

import numpy as np
import pandas as pd  # Because I'm lazy
from natsort import os_sorted
from tqdm import tqdm


def gen_column_names() -> str:
    alphabets = [chr(ord('A') + i) for i in range(10)]
    for i, a in zip(chain(range(2, 6), [5, 5], range(5, 1, -1)), alphabets):
        for j in range(i, 0, -1):
            yield f'{a}M{j}'
        for j in range(1, i + 1):
            yield f'{a}{j}'


def gen_indices() -> Tuple[int, int]:
    for i, start_idx in zip(range(10), chain(range(3, -1, -1), [0, 0], range(4))):
        length = 2 * (5 - start_idx)
        for _ in range(length):
            yield (i, start_idx)
            start_idx += 1


column_names = list(gen_column_names())
cell_indices = list(gen_indices())


def dict_from_csv(filename) -> Dict[str, str]:
    d = {'filename': f'{filename.stem}.jpg'}
    mat = np.loadtxt(filename, dtype=object, ndmin=2, delimiter=',')
    for (i, j), column_name in zip(cell_indices, column_names):
        d[column_name] = mat[i, j]
    return d


def show_name_to_cell_mappings():
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import ImageGrid

    fig = plt.figure(figsize=(10, 10))
    grid = ImageGrid(
        fig,
        111,  # similar to subplot(111)
        nrows_ncols=(10, 10),
        axes_pad=0.0,
        share_all=True,
    )
    def idxer(u): return grid[u[0] * 10 + u[1]]
    for ij, name in zip(cell_indices, column_names):
        ax = idxer(ij)
        ax.text(.5, .5, name, horizontalalignment='center', verticalalignment='center',
                transform=ax.transAxes, size='xx-large', fontfamily='Noto Sans Mono')
        ax.set_xticks([])
        ax.set_yticks([])
    plt.savefig('name_to_cell_mappings.png',
                bbox_inches='tight', pad_inches=0.2)
    # plt.show()


def main():
    files = Path('../OUTPUT_MAIN_DIAG')
    filenames = os_sorted(files.glob('**/*.csv'), )
    rows_list = [dict_from_csv(filename) for filename in tqdm(filenames)]
    df = pd.DataFrame(rows_list)
    df.to_csv('combined_main_diagram_0.0.3.csv', index=False)


if __name__ == '__main__':
    main()
    # show_name_to_cell_mappings()
